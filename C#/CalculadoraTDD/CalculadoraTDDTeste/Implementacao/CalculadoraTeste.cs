﻿using CalculadoraTDD.Exceptions;
using CalculadoraTDD.Implementacao;
using CalculadoraTDD.Interfaces;
using FluentAssertions;
using NUnit.Framework;
using System;

namespace CalculadoraTDDTeste.Implementacao
{
    public class CalculadoraTeste
    {
        private ICalculadora calculadora;
        private decimal valorInicial;

        [SetUp]
        public void DefinirBaseParaTestes()
        {
            valorInicial = 10;
            calculadora = new Calculadora(valorInicial);
        }

        [Test]
        public void AoObterResultadoSemExecutarNemUmaOperacaoRetornoValorInicial()
        {
            // Arrange

            // Act
            var resultado = calculadora.ObterResultado();

            // Assert
            resultado.Should().Be(valorInicial);
        }

        [Test]
        public void DeveSomarValorInformadoAoResultadoDaCalculadora()
        {
            // Arrange
            var valorAhSerSomado = 5;
            var resultadoEsperado = valorInicial + valorAhSerSomado;

            // Act
            calculadora.Somar(valorAhSerSomado);

            // Assert
            var resultado = calculadora.ObterResultado();
            resultado.Should().Be(resultadoEsperado);
        }

        [Test]
        public void DeveSubtrairValorInformadoDoResultadoDaCalculadora()
        {
            // Arrange
            var valorAhSerSubtraido = 5;
            var resultadoEsperado = valorInicial - valorAhSerSubtraido;

            // Act
            calculadora.Subtrair(valorAhSerSubtraido);

            // Assert
            var resultado = calculadora.ObterResultado();
            resultado.Should().Be(resultadoEsperado);
        }

        [Test]
        public void DeveDividirPeloValorInformadoDoResultadoDaCalculadora()
        {
            //Arrange
            var divisor = 2;
            var resultadoEsperado = valorInicial / divisor;

            //Act
            calculadora.Dividir(divisor);

            //Assert
            var resultado = calculadora.ObterResultado();
            resultado.Should().Be(resultadoEsperado);
        }

        [Test]
        public void DeveMultiplicarOValorAcumuladoDaCalculadoraPeloValorInformado()
        {
            //Arrang
            var fator = 2;
            var resultadoEsperado = valorInicial * fator;

            //Act
            calculadora.Multiplicar(fator);

            //Assert
            var resultado = calculadora.ObterResultado();
            resultado.Should().Be(resultadoEsperado);
        }

        [TestCase("2 * 2 + 3", 7)]
        [TestCase("2 / 2 + 3", 4)]
        [TestCase("2 - 2 * 3", 0)]
        [TestCase("20 + 10 + 20", 50)]
        public void DeveCalcularOResultadoDaExpresao(string expresao, int resultadoEsperado)
        {
            //Arrang

            //Act
            var resultado = calculadora.CalcularExpresao(expresao);

            //Assert

            resultado.Should().Be(resultadoEsperado);
        }

        [Test]
        public void QuandoForInformadoUmaExpressaoInvalidaDeLancaException()
        {
            //Arrang
            var expressao = "20x * 10";

            //Act
            Action acao = () => calculadora.CalcularExpresao(expressao);

            //Assert
            acao.Should().Throw<ExpressaoInvalidaException>();
        }

        /*
         [Test]
        public void teste()
        {
            //Arrang

            //Act

            //Assert
        }
        */
    }
}