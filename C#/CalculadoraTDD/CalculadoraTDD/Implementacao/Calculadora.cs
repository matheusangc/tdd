﻿using CalculadoraTDD.Exceptions;
using CalculadoraTDD.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CalculadoraTDD.Implementacao
{
    public class Calculadora : ICalculadora
    {
        private decimal resultado;
        private readonly List<char> operadores;
        private readonly List<char> numeoros;
        private readonly Dictionary<char, Func<int, int, int>> menuDeOperacoes;

        public Calculadora(decimal valorInicial)
        {
            operadores = new List<char>() { '*', '/', '+', '-' };
            numeoros = new List<char>() { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            menuDeOperacoes = new Dictionary<char, Func<int, int, int>>
            {
                { operadores[0], (x, y) => x * y },
                { operadores[1], (x, y) => x / y },
                { operadores[2], (x, y) => x + y },
                { operadores[3], (x, y) => x - y }
            };

            resultado = valorInicial;
        }

        public decimal ObterResultado()
        {
            return resultado;
        }

        public ICalculadora Somar(decimal parcela)
        {
            resultado += parcela;
            return this;
        }

        public ICalculadora Subtrair(decimal subtraendo)
        {
            resultado -= subtraendo;
            return this;
        }

        public ICalculadora Dividir(decimal divisor)
        {
            resultado /= divisor;
            return this;
        }

        public ICalculadora ElevarAhPotenciaDe(decimal expoente)
        {
            throw new System.NotImplementedException();
        }

        public ICalculadora Multiplicar(decimal fator)
        {
            resultado *= fator;
            return this;
        }

        public decimal CalcularExpresao(string expressao)
        {
            var exp = FormatarStringExpressao(expressao);

            var resultado = ObterResultadoDaExpresao(exp);

            return resultado;
        }

        public ICalculadora Raiz(decimal indice)
        {
            throw new System.NotImplementedException();
        }

        public ICalculadora RaizQuadrada()
        {
            throw new System.NotImplementedException();
        }

        private int ObterResultadoDaExpresao(string[] exp)
        {
            var valorBase = 0;
            char? operacao = null;
            foreach (var item in exp)
            {
                if (item.Count() == 1 && operadores.Contains(ToChar(item)))
                {
                    operacao = ToChar(item);
                    continue;
                }
                else if (operacao.HasValue)
                {
                    valorBase = menuDeOperacoes[operacao.Value](valorBase, Convert.ToInt32(item));
                    operacao = null;
                }
                else
                {
                    valorBase = Convert.ToInt32(item);
                }
            }

            return valorBase;
        }

        private char ToChar(string s)
        {
            return s.ToCharArray().FirstOrDefault();
        }

        private string[] FormatarStringExpressao(string exp)
        {
            exp = exp.Replace(" ", string.Empty);
            var expresao = exp.ToCharArray().ToList();
            return ConverterExpressao(expresao).ToArray();
        }

        private List<string> ConverterExpressao(List<char> expressao)
        {
            var retorno = new List<string>();
            string elemento = string.Empty;
            foreach (var item in expressao)
            {
                if (!EhElementoValido(item))
                    throw new ExpressaoInvalidaException();

                if (operadores.Contains(item))
                {
                    retorno.Add(elemento);
                    retorno.Add(item.ToString());
                    elemento = string.Empty;
                    continue;
                }
                elemento += item;
            }
            retorno.Add(elemento);

            return retorno;
        }

        private bool EhElementoValido(char item)
        {
            return operadores.Contains(item) || numeoros.Contains(item);
        }
    }

    public enum TipoCaractereEnum
    {
        Invalido = 1,
        Operador = 2,
        Numerico = 3,
    }
}