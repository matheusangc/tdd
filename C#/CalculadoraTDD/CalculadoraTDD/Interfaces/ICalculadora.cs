﻿namespace CalculadoraTDD.Interfaces
{
    public interface ICalculadora
    {
        ICalculadora Somar(decimal parcela);

        ICalculadora Subtrair(decimal subtraendo);

        ICalculadora Multiplicar(decimal fator);

        ICalculadora Dividir(decimal divisor);

        ICalculadora ElevarAhPotenciaDe(decimal expoente);

        ICalculadora Raiz(decimal indice);

        ICalculadora RaizQuadrada();

        decimal ObterResultado();

        decimal CalcularExpresao(string expressao);
    }
}