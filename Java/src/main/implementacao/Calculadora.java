package main.implementacao;

import main.interfaces.ICalculadora;

public class Calculadora implements ICalculadora {
    private double _resultado;

    public Calculadora(double valorInicial){
        _resultado = valorInicial;

    }

    @Override
    public ICalculadora Somar(double parcela) {
        _resultado += parcela;
        return this;
    }

    @Override
    public ICalculadora Subtrair(double subtraendo) {
    	_resultado -= subtraendo;
        return this;
    }

    @Override
    public ICalculadora Multiplicar(double fator) {
    	_resultado *= fator;
        return this;
    }

    @Override
    public ICalculadora Dividir(double divisor) {
    	_resultado /= divisor;
        return this;
    }

    @Override
    public ICalculadora ElevarAhPotenciaDe(double expoente) {
        return this;
    }

    @Override
    public ICalculadora Raiz(double indice) {
        return this;
    }

    @Override
    public ICalculadora RaizQuadrada() {
        return this;
    }

    @Override
    public double ObterResultado() {
        return _resultado;
    }

    @Override
    public double CalcularExpresao(String expressao) {
        return 0;
    }
}
