package main.interfaces;

public interface ICalculadora {
    ICalculadora Somar(double parcela);

    ICalculadora Subtrair(double subtraendo);

    ICalculadora Multiplicar(double fator);

    ICalculadora Dividir(double divisor);

    ICalculadora ElevarAhPotenciaDe(double expoente);

    ICalculadora Raiz(double indice);

    ICalculadora RaizQuadrada();

    double ObterResultado();

    double CalcularExpresao(String expressao);
}