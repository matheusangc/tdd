package testes.implementacao;

import main.implementacao.Calculadora;
import main.interfaces.ICalculadora;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculadoraTeste {
    private final double _valorInicial = 20.0;
    private ICalculadora _calculadora;


    @Before
    public void ConfiguracoesBase(){
        _calculadora = new Calculadora(_valorInicial);
    }

    @Test
    public void quandoNaoEfetuarNemUmaOperacaoDeveRetornarOValorInicial() {
        // Arrange

        // Act
        double retorno = _calculadora.ObterResultado();

        // Assert
        Assert.assertEquals(retorno, _valorInicial, 0);
    }

    @Test
    public void deveSomarValorAoResultado() {
        // Arrange
    	double valor = 10;
        double retornoEsperado = _valorInicial + valor;

        // Act
        double retorno = _calculadora.Somar(10).ObterResultado();

        // Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }

    @Test
    public void deveSubtrairValorDoResultado() {
    	// Arrange
    	double valor = 3;
    	double retornoEsperado = _valorInicial - valor;
    	
    	// Act
    	double retorno = _calculadora.Subtrair(valor).ObterResultado();
    	
    	// Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }

    @Test
    public void deveMultiplicarPeloValorOResultado() {
    	// Arrange
    	double valor = 50;
    	double retornoEsperado = _valorInicial * valor;    	
    	
    	// Act
    	double retorno = _calculadora.Multiplicar(valor).ObterResultado();
    	
    	// Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }

    @Test
    public void deveDividirPeloValorOResultado() {
    	// Arrange
    	double valor = 2;
    	double retornoEsperado = _valorInicial / valor;
    	
    	// Act
    	double retorno = _calculadora.Dividir(valor).ObterResultado();
    	
    	// Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }

    @Test
    public void deveElevarAPotenciaInformadaOResultado() {
    	// Arrange
    	double valor = 3;
    	double retornoEsperado = Math.pow(_valorInicial, valor);
    	
    	// Act
    	double retorno = _calculadora.ElevarAhPotenciaDe(valor).ObterResultado();
    	
    	// Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }

    @Test
    public void deveObterRaizInformadoDoResultado() {
    	// Arrange
    	double valor = 3;
    	_calculadora.Somar(709);
    	double retornoEsperado = 9;
    	
    	// Act
    	double retorno = _calculadora.Raiz(valor).ObterResultado();
    	
    	// Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }

    @Test
    public void deveObterRaizQuadradaDoResultado() {
    	// Arrange
    	_calculadora.Somar(61);
    	double retornoEsperado = Math.sqrt(_calculadora.ObterResultado());
    	
    	// Act
    	double retorno = _calculadora.RaizQuadrada().ObterResultado();
    	
    	// Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }


    @Test
    public void deveObterOResultadoDaExpresaoInformada() {
    	// Arrange
    	String expresao = "4 * 10 / 5";
    	double retornoEsperado = 8;
    	
    	// Act
    	double retorno = _calculadora.CalcularExpresao(expresao);
    	
    	// Assert
        Assert.assertEquals(retornoEsperado, retorno, 0);
    }
}